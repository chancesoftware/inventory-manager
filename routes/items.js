const express = require('express')
const router = express.Router()

// Get All Items
router.get('/', (req, res) => {
    res.send('Get Route is Called!')
})

// Get One Item
router.get('/:id', (req, res) => {
    
})

// Create One Item
router.post('/', (req, res) => {
    
})

// Update One Item
router.patch('/:id', (req, res) => {
    
})

// Delete One Item
router.delete('/:id', (req, res) => {
    
})

module.exports = router