// Loads environment variables from .env
require('dotenv').config()

// Set Up Express Server
const express = require('express')
const app = express()

// Configure Mongoose to connect to MongoDB
const mongoose = require('mongoose')

mongoose.connect(process.env.DATABASE_URL)
const db = mongoose.connection

// Check to see if there are any errors when connecting to database
db.on('error', (error) => console.error(error))
db.once('open', (error) => console.log('Successfully connected to database!'))

// app.use code will run when the server gets a request, but before it is passed to our routes
// This allows us to pass json objects in the request
app.use(express.json())

const inventoryRouter = require('./routes/items')
app.use('/items', inventoryRouter)


app.listen(3000, () => console.log('Server has started!'))